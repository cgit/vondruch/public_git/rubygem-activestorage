# Generated from activestorage-0.1.gem by gem2rpm -*- rpm-spec -*-
%global gem_name activestorage

Name: rubygem-%{gem_name}
Version: 0.1
Release: 1%{?dist}
Summary: Attach cloud and local files in Rails applications
License: MIT
# TODO: This was merged into rails, please change the URL witn next version.
URL: https://github.com/rails/activestorage
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby >= 2.3.0
BuildRequires: rubygem(actionpack)
BuildRequires: rubygem(activejob)
BuildRequires: rubygem(activerecord)
BuildRequires: rubygem(activesupport)
BuildRequires: rubygem(sqlite3)
BuildArch: noarch

%description
Attach cloud and local files in Rails applications.


%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
%setup -q -n  %{gem_name}-%{version}

%build
# Create the gem as gem install only works on a gem file
gem build ../%{gem_name}-%{version}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/



%check
pushd .%{gem_instdir}
# Get rid of Bundler.
sed -i '/require "bundler/ s/^/#/' test/test_helper.rb

# ByeBug is just debuging tool.
sed -i '/require "byebug"/ s/^/#/' test/test_helper.rb

# Make the tests pass without service configuration. This appears to be fixed
# upstream.
# https://github.com/rails/rails/commit/255b1a149c8be93ede25404c53933bde3acc2dc2
sed -i '/puts/a\  {}' test/service/shared_service_tests.rb

ruby -Ilib:test -e "Dir.glob('./test/**/*_test.rb').sort.each {|t| require t}"
popd

%files
%dir %{gem_instdir}
%exclude %{gem_instdir}/.gitignore
%license %{gem_instdir}/MIT-LICENSE
%exclude %{gem_instdir}/activestorage.gemspec
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/Gemfile
%{gem_instdir}/Gemfile.lock
%doc %{gem_instdir}/README.md
%{gem_instdir}/Rakefile
%{gem_instdir}/test
%exclude %{gem_instdir}/test/service/.*

%changelog
* Mon Aug 28 2017 Vít Ondruch <vondruch@redhat.com> - 0.1-1
- Initial package
